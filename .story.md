---
focus: src/App.js
---
### Updating data in the hooks

So, now we have created hooks for all checkboxes and the next step is updating the value after each checkbox changes.

How we can do it? For each element we should add two attributes: `checked` and `onChange`.

`checked` - we should use this attribute for setting the right state of the checkboxes.
`onChange` - the function is called whenever the state of this checkbox was changed.

Don't worry about the sorting function, it's just a typical sorting function.

[highlight](package.json:1-5)

[highlight](package.json:5)

[open package.json file](package.json)
